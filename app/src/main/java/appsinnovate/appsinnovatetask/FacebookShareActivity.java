package appsinnovate.appsinnovatetask;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacebookShareActivity extends AppCompatActivity implements View.OnClickListener {

    private CallbackManager callbackManager;
    private LoginManager manager;
    @BindView(R.id.take_new_picture)
    Button takeNewPicture;
    @BindView(R.id.select_existing_picture)
    Button selectExistingPicture;
    Boolean pictureType;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 12345;
    String mCurrentPhotoPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_facebook_share);
            ButterKnife.bind(this);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            /*Checks for Camera  & gallery access permission*/
            if (Build.VERSION.SDK_INT < 23) {
            } else {
                CheckForCameraPermission();

            }
            takeNewPicture.setOnClickListener(this);
            selectExistingPicture.setOnClickListener(this);

        }
    }

    /*Handles the share after bitmap is returned*/
    public void sharePhotoToFacebook(Bitmap bitmap) {
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("Just shared from AppsInnovateTask app ")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        ShareDialog shareDialog = new ShareDialog(FacebookShareActivity.this);

        shareDialog.show(content);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_existing_picture:
                loginAndSharePicture(false);
                break;
            case R.id.take_new_picture:
                loginAndSharePicture(true);
                break;
        }
    }

    public void loginAndSharePicture(final Boolean pictureType) {
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        /*Permission Set */
        List<String> permissionNeeds = Arrays.asList("publish_actions");

        //this loginManager helps you eliminate adding a LoginButton to your UI
        manager = LoginManager.getInstance();

        manager.logInWithPublishPermissions(this, permissionNeeds);

        manager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (pictureType) {

                    dispatchTakePictureIntent();
                } else {
                    Intent takePicture = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(takePicture, 0);//zero can be replaced with any action code

                }
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException error) {

            }


        });
    }

    /**
     * Calls a function to check for the sms and phone permission to handle it at run time incase of Marshamellow +
     */
    public void CheckForCameraPermission() {
        if (Build.VERSION.SDK_INT < 23) {
        } else {
            requestCameraPermission();
        }
    }

    private void requestCameraPermission() {

        int hasCameraPermission = ActivityCompat.checkSelfPermission(FacebookShareActivity.this, Manifest.permission.CAMERA);
        int hasPhonePermission = ActivityCompat.checkSelfPermission(FacebookShareActivity.this, Manifest.permission.READ_PHONE_STATE);

        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FacebookShareActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1234);
        }
    }

    //Grants the permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "SMS & Phone permission has now been granted. Showing result.");
                    //    Toast.makeText(getActivity(), "SMS Permission is Granted", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i("Permission", "SMS & Phone permission were NOT granted.");
                }
                break;
        }
    }

    //From android developers google
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /*Gets the  image URI after being saved from the camera */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "karim.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (responseCode == RESULT_OK) {
                    if (!mCurrentPhotoPath.isEmpty()) {
                        /*Decode image saved in the photopath and send it to the share method*/
                        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                        sharePhotoToFacebook(bitmap);
                    }

                }
                break;
            case 0:
                if (responseCode == RESULT_OK) {
                    /*Gets the bitmap from URI and share it */
                    Uri imageUri = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        sharePhotoToFacebook(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

