package appsinnovate.appsinnovatetask;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import Adapters.LoadCountriesAdapter;
import Utilities.CountriesAPI;
import Data.Country.Country;
import Utilities.ApiClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchCountryActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.countries_recycler)
    RecyclerView counrtiesRecycler;
    @BindView(R.id.countries_search_view)
    EditText countrySearch;
    @BindView(R.id.execute_search)
    Button executeSearch;
    LoadCountriesAdapter loadCountriesAdapter;
    public ArrayList<Country> countryList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_country);
        ButterKnife.bind(this);
        executeSearch.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchCountryActivity.this);
        counrtiesRecycler.setLayoutManager(linearLayoutManager);
        loadCountriesAdapter = new LoadCountriesAdapter(getApplicationContext());

    }
    public ArrayList<Country> makeApiCall() {
        CountriesAPI countriesAPI = ApiClient.getClient().create(CountriesAPI.class);
        Call<ArrayList<Country>> requestCountries = countriesAPI.getCountries(countrySearch.getText().toString(), "name;flag");

        requestCountries.enqueue(new Callback<ArrayList<Country>>() {
            @Override
            public void onResponse(Call<ArrayList<Country>> call, Response<ArrayList<Country>> response) {
                countryList = response.body();
                if(countryList!=null) {
                    loadCountriesAdapter.setCountryList(countryList);
                    counrtiesRecycler.setAdapter(loadCountriesAdapter);
                    loadCountriesAdapter.notifyDataSetChanged();
                    hideKeyboard();
                }else {
                    Toast.makeText(SearchCountryActivity.this, "There were no search results that matches your request , please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Country>> call, Throwable t) {

            }
        });
        return countryList;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.execute_search:
                if(countrySearch.getText().length()==0||countrySearch.getText().toString().isEmpty())
                {
                    countrySearch.requestFocus();
                    countrySearch.setError("Country Name is empty");
                    executeSearch.startAnimation(AnimationUtils.loadAnimation(SearchCountryActivity.this, R.anim.shake_error));

                }
                else
                {
                    makeApiCall();

                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void hideKeyboard(){
        View view = SearchCountryActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
