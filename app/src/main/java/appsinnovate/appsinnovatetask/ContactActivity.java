package appsinnovate.appsinnovatetask;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import Adapters.LoadContactsAdapter;
import Adapters.LoadCountriesAdapter;
import Data.Contact;
import Utilities.PermissionUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.contact_load_time)
    TextView contactLoadTime;
    @BindView(R.id.load_contacts_activity_button)
    Button loadContacts;
    @BindView(R.id.contacts_recycler)
    RecyclerView contactsRecycler;
    LoadContactsAdapter loadContactsAdapter;
    ArrayList<Contact> contacts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        /*Check Permission incase of Marshamellow +*/
        if (Build.VERSION.SDK_INT < 23) {
        }
        else
        {
            checkPermission();

        }
        loadContacts.setOnClickListener(this);


    }
    /*Gets Contacts and returns the time of contact fetch*/
    void getAllContacts() {
        long startnow;
        long endnow;

        startnow = android.os.SystemClock.uptimeMillis();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,   ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.Contacts._ID}, selection, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        cursor.moveToFirst();
        contacts=new ArrayList<>();
        while (!cursor.isAfterLast()) {
            Contact contact=new Contact();
            contact .setName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            contact .setPhone( cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
           contacts.add(contact);
            cursor.moveToNext();
        }
        cursor.close();

        endnow = android.os.SystemClock.uptimeMillis();
        contactLoadTime.setText("Time taken to load contacts " + (endnow - startnow) + " ms");
        Log.d("END", "TimeForContacts " + (endnow - startnow) + " ms");
    }


public  void checkPermission(){

    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS)
            != PackageManager.PERMISSION_GRANTED) {
        // Permission to access the Contacts is missing.
        PermissionUtils.requestPermission(this, 1,
                Manifest.permission.READ_CONTACTS, true);
    }
}

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.load_contacts_activity_button){
            getAllContacts();
            /*Load the RecyclerView*/
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ContactActivity.this);
            contactsRecycler.setLayoutManager(linearLayoutManager);
            loadContactsAdapter = new LoadContactsAdapter(getApplicationContext());
            loadContactsAdapter.setContacts(contacts);
            contactsRecycler.setAdapter(loadContactsAdapter);
            loadContactsAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
