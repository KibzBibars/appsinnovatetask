package appsinnovate.appsinnovatetask;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.startYear;

public class CallendarActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.event_name)
    EditText eventName;
    @BindView(R.id.event_description)
    EditText eventDescription;
    @BindView(R.id.event_email)
    EditText eventEmail;
    @BindView(R.id.event_location_name)
    EditText eventLocationName;
    @BindView(R.id.create_event)
    Button createEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callendar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        createEvent.setOnClickListener(this);

    }
    /*Executes intent to call calendar with parameters from the Edittexts*/
    public void executeCalendar(){

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.Events.TITLE, eventName.getText().toString())
                .putExtra(CalendarContract.Events.DESCRIPTION, eventDescription.getText().toString())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, eventLocationName.getText().toString())
                .putExtra(Intent.EXTRA_EMAIL, eventEmail.getText().toString());
        startActivityForResult(intent,1);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.create_event){
            executeCalendar();
        }
    }

    //TODO: Try to make the app return after event is created
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
