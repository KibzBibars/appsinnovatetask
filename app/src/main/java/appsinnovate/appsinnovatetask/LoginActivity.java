package appsinnovate.appsinnovatetask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    CallbackManager  callbackManager;
    @BindView(R.id.loadfriends)
    Button loadFriends;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.country_search)
    Button countrySearch;
    @BindView(R.id.load_contacts_button)
    Button loadContacts;
    @BindView(R.id.share_image_button)
    Button shareImageToFacebook;
    @BindView(R.id.calendar_button)
    Button calendarButton;
    @BindView(R.id.maps_button)
    Button mapsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        /*Setting Permissions for facebook Button*/
        loginButton.setReadPermissions("user_friends");
        callbackManager = CallbackManager.Factory.create();

        /*Setting ClickListerners*/
        loadFriends.setOnClickListener(this);
        countrySearch.setOnClickListener(this);
        loadContacts.setOnClickListener(this);
        shareImageToFacebook.setOnClickListener(this);
        calendarButton.setOnClickListener(this);
        mapsButton.setOnClickListener(this);

        /*Checking if I am already logged in to enable or disable the Show user list */
        if(checkIfLoggedin()){
            loadFriends.setEnabled(true);
        }
        else
        {
            loadFriends.setEnabled(false);
        }

        /*Login Button Callback*/
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(LoginActivity.this, "Logged in successfully"
                        , Toast.LENGTH_SHORT).show();
                //Set show user list enabled in case of success
                loadFriends.setEnabled(true);
            }

            @Override
            public void onCancel() {
                loadFriends.setEnabled(false);

            }

            @Override
            public void onError(FacebookException error) {
                loadFriends.setEnabled(false);
            }
        });





    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loadfriends:
                if(checkIfLoggedin()){
                    Intent loginIntent = new Intent(LoginActivity.this, UserListActivity.class);
                    startActivity(loginIntent);
                }
                else
                {
                    Toast.makeText(this, "Please Login First", Toast.LENGTH_SHORT).show();
                    loadFriends.setEnabled(false);
                }
                break;
            case R.id.country_search:
                Intent countrySearchIntent = new Intent(LoginActivity.this, SearchCountryActivity.class);
                startActivity(countrySearchIntent);
                break;
            case R.id.load_contacts_button:
                Intent loadContactsIntent = new Intent(LoginActivity.this, ContactActivity.class);
                startActivity(loadContactsIntent);
                break;
            case R.id.share_image_button:
                Intent loadShareIntent = new Intent(LoginActivity.this, FacebookShareActivity.class);
                startActivity(loadShareIntent);
                break;
            case R.id.calendar_button:
                Intent loadCalendarIntent = new Intent(LoginActivity.this, CallendarActivity.class);
                startActivity(loadCalendarIntent);
                break;
            case R.id.maps_button:
                Intent mapsIntent = new Intent(LoginActivity.this, MapsActivity.class);
                startActivity(mapsIntent);
                break;
        }
    }
    /*Checks if the access token is null ((user logged in or not )*/
    public boolean checkIfLoggedin(){
            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            return accessToken != null;

    }
}
