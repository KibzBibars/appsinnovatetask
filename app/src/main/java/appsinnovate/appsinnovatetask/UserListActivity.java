package appsinnovate.appsinnovatetask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Adapters.LoadUserAdapter;
import Data.Friend.Friend;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends AppCompatActivity {
    Friend friend = new Friend();

    @BindView(R.id.user_recycler_view)
    RecyclerView userRecyclerView;

    LoadUserAdapter loadUserAdapter;
    AsyncTask asyncTask;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = ProgressDialog.show(UserListActivity.this, "", "Fetching users please wait");
        progressDialog.show();


        asyncTask = new AsyncTask() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                fetchUserInfo();
                return true;
            }

            @Override
            protected void onPostExecute(Object o) {
                progressDialog.dismiss();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UserListActivity.this);
                userRecyclerView.setLayoutManager(linearLayoutManager);
                loadUserAdapter = new LoadUserAdapter(getApplicationContext());
                loadUserAdapter.setFriend(friend);
                userRecyclerView.setAdapter(loadUserAdapter);
                loadUserAdapter.notifyDataSetChanged();
            }

        };
        asyncTask.execute();


    }


    public void fetchUserInfo() {


        final String[] afterString = {""};  // will contain the next page cursor
        final Boolean[] noData = {false};   // stop when there is no after cursor
        do {
            Bundle params = new Bundle();
            params.putString("after", afterString[0]);
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/taggable_friends?fields=first_name,last_name,middle_name,picture.type(large)",
                    params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse graphResponse) {
                            JSONObject jsonObject = graphResponse.getJSONObject();
                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                //  your code
                                Gson gson = new Gson();
                                Friend friendtemp = new Friend();
                                friendtemp = gson.fromJson(graphResponse.getJSONObject().toString(), Friend.class);
                                if (friend == null) {
                                    friend = friendtemp;
                                } else {
                                    friend.getData().addAll(friendtemp.getData());

                                }
                                Log.e("trial", "" + friend.getData().size());

                                if (!jsonObject.isNull("paging")) {
                                    JSONObject paging = jsonObject.getJSONObject("paging");
                                    JSONObject cursors = paging.getJSONObject("cursors");
                                    if (!cursors.isNull("after"))
                                        afterString[0] = cursors.getString("after");
                                    else
                                        noData[0] = true;
                                } else
                                    noData[0] = true;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAndWait();
        }
        while (!noData[0]);
        Log.e("Friend", friend.toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
            return super.onOptionsItemSelected(item);
        }
    }
}
