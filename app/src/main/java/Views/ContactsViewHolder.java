package Views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import Adapters.LoadContactsAdapter;
import Adapters.LoadCountriesAdapter;
import appsinnovate.appsinnovatetask.R;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by tarek on 10/02/17.
 */

public class ContactsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    @BindView(R.id.contact_name)
    TextView contactName;
    @BindView(R.id.contact_phone)
    TextView contactPhone;

    private  Context context;
    private int position;

    LoadContactsAdapter.ClickListener clickListener;

    public ContactsViewHolder(View itemView, Context context, LoadContactsAdapter.ClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.clickListener=clickListener;
        this.context=context;
        itemView.setOnClickListener(this);

    }


    public void setViewHolderFields(String name , String phone) {


        contactName.setText(name);
        contactPhone.setText(phone);

    }
    @Override
    public void onClick(View view) {
        if(clickListener!=null) {
            clickListener.onClick(getAdapterPosition());
        }

    }
}
