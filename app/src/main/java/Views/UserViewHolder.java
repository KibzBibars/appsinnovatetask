package Views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import Adapters.LoadUserAdapter;
import appsinnovate.appsinnovatetask.R;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by tarek on 10/02/17.
 */

public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_image)
    ImageView userImage;

    private  Context context;
    private int position;

    LoadUserAdapter.ClickListener clickListener;

    public UserViewHolder(View itemView, Context context,LoadUserAdapter.ClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.clickListener=clickListener;
        this.context=context;
        itemView.setOnClickListener(this);

    }


    public void setViewHolderFields(String firstName,String middleName,String lastName,String imageURL) {

        Picasso.with(context)
                .load(imageURL)
/*
                .error(R.drawable.loading)
*/
                .into(userImage);
        if(middleName==null)
        {
            userName.setText(""+firstName+" "+lastName);
        }
        else
        {
            userName.setText(firstName+" "+middleName+" "+lastName);

        }
    }
    @Override
    public void onClick(View view) {
        if(clickListener!=null) {
            clickListener.onClick(getAdapterPosition());
        }

    }
}
