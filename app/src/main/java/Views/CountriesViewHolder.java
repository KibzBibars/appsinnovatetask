package Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.StreamEncoder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.io.InputStream;

import Adapters.LoadCountriesAdapter;
import Adapters.LoadUserAdapter;
import appsinnovate.appsinnovatetask.R;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by tarek on 10/02/17.
 */

public class CountriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    @BindView(R.id.country_name)
    TextView countryName;
/*    @BindView(R.id.country_image)
    ImageView countryImage;*/
    GenericRequestBuilder requestBuilder;

    private  Context context;
    private int position;

    LoadCountriesAdapter.ClickListener clickListener;

    public CountriesViewHolder(View itemView, Context context, LoadCountriesAdapter.ClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.clickListener=clickListener;
        this.context=context;
        itemView.setOnClickListener(this);

    }


    public void setViewHolderFields(String countryNameString,String countryFlagURL) {


        countryName.setText(countryNameString);
/*       // ImageSize targetSize = new ImageSize(80, 50); // result Bitmap will be fit to this size
        ImageLoader imageLoader=ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));

        imageLoader.loadImage(countryFlagURL, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                countryImage.setImageBitmap(loadedImage);
            }
        });*/

    }
    @Override
    public void onClick(View view) {
        if(clickListener!=null) {
            clickListener.onClick(getAdapterPosition());
        }

    }


}
