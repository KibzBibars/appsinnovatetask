package Data.Country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kbibars on 5/2/17.
 */

public class Country {
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("relevance")
    @Expose
    private String relevance;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelevance() {
        return relevance;
    }

    public void setRelevance(String relevance) {
        this.relevance = relevance;
    }
}
