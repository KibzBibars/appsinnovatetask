package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Data.Contact;
import Data.Country.Country;
import Views.ContactsViewHolder;
import Views.CountriesViewHolder;
import appsinnovate.appsinnovatetask.R;

/**
 * Created by kbibars on 5/2/17.
 */

public class LoadContactsAdapter extends RecyclerView.Adapter<ContactsViewHolder> {
    ArrayList<Contact> contacts;
    Context context;

    public interface ClickListener {
        void onClick(int index);
    }
    public   ClickListener clickListener;

    public LoadContactsAdapter(Context context) {
        this.context=context;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_contact, parent, false);

        return new ContactsViewHolder(itemView,context,clickListener);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {
    holder.setViewHolderFields(contacts.get(position).getName(),contacts.get(position).getPhone());
    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }
}
