package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import Data.Country.Country;
import Data.Friend.Datum;
import Data.Friend.Friend;
import Views.CountriesViewHolder;
import Views.UserViewHolder;
import appsinnovate.appsinnovatetask.R;

/**
 * Created by kbibars on 5/2/17.
 */

public class LoadCountriesAdapter extends RecyclerView.Adapter<CountriesViewHolder> {
    ArrayList<Country> countryList;
    Context context;

    public interface ClickListener {
        void onClick(int index);
    }
    public   ClickListener clickListener;

    public LoadCountriesAdapter(Context context) {
        this.context=context;
    }

    public void setCountryList(ArrayList<Country> countryList) {
        this.countryList = countryList;
    }

    @Override
    public CountriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_country, parent, false);

        return new CountriesViewHolder(itemView,context,clickListener);
    }

    @Override
    public void onBindViewHolder(CountriesViewHolder holder, int position) {
    holder.setViewHolderFields(countryList.get(position).getName(),countryList.get(position).getFlag());
    }


    @Override
    public int getItemCount() {
        return countryList.size();
    }
}
