package Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import Data.Friend.Datum;
import Data.Friend.Friend;
import Views.UserViewHolder;
import appsinnovate.appsinnovatetask.R;

/**
 * Created by kbibars on 5/2/17.
 */

public class LoadUserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    Friend friend;
    Context context;

    public interface ClickListener {
        void onClick(int index);
    }
    public   ClickListener clickListener;

    public LoadUserAdapter( Context context) {
        this.context=context;
    }

    public void setFriend(Friend friend) {
        this.friend = friend;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_user, parent, false);

        return new UserViewHolder(itemView,context,clickListener);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        Datum datum=friend.getData().get(position);
        holder.setViewHolderFields(datum.getFirstName(),datum.getMiddleName(),datum.getLastName(),datum.getPicture().getData().getUrl());
    }

    @Override
    public int getItemCount() {
        return friend.getData().size();
    }
}
