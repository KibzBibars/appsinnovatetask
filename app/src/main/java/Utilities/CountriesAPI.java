package Utilities;

import java.util.ArrayList;

import Data.Country.Country;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by kbibars on 5/2/17.
 */

public interface CountriesAPI {

    @GET("name/{name}")
    Call<ArrayList<Country>> getCountries(@Path("name") String countryName, @Query("fields")String name);

}
